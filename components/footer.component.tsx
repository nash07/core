import React, { Component } from 'react';

export class FooterComponent extends Component {
  
    render() {
        return (
            <footer>
                <p>
                    © 2020 React Designtool. All rights reserved. Privacy Policy
                </p>
            </footer>
        );
    }
}

