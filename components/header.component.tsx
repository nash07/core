import React, { Component } from 'react';

export class HeaderComponent extends Component {
  
    render() {
        return (
            <header>
                <nav role="navigation">
                    <ul>
                        <li>
                            <a>
                                    Home
                            </a>
                        </li>
                        <li>
                            <a>
                                    Blog
                            </a>
                            <div>
                                <ul>
                                    <li><a href="#">Me</a></li>
                                    <li><a href="#">Gaming</a></li>
                                    <li><a href="#">Sport</a></li>
                                    <li><a href="#">Web Design</a></li>
                                    <li><a href="#">Web Development</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </nav>
            </header>
        );
    }
}

